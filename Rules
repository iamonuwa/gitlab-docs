#!/usr/bin/env ruby

preprocess do
  @items.each do |item|
    if item.identifier.to_s.end_with?(".md") && !item.binary?
      # If the page is a redirect, make it a redirect.
      if item.raw_content =~ /^This document was moved to \[.*\]\(.*\)/m
        # Capture the intended page so the redirect page can redirect to it.
        item[:redirect] = item.raw_content.match(/^This document was moved to \[.*\]\((.*)\)/m)[1]
        # Correct the URL.
        item[:redirect] = item[:redirect].gsub!(/\.md/, '.html')
      end

      unless item[:title]
        title = item.raw_content.match(/^[#] .*$/).to_s
        title.gsub!('# ', '')
        item[:title] = title unless title.empty?
      end
    end
  end
end

compile '/404.*' do
  filter :erb
  layout '/404.*'
  write '/404.html'
end

compile '/**/*.html' do
  layout '/default.*'
end

compile '/**/*.md' do
  if item[:redirect].nil?

    # If 'toc' is absent in a file's yaml frontmatter, show ToC.
    # Set to 'toc: false' to disable it.
    include_toc = item[:toc].nil? ? true : false

    # Use Redcarpet with Rouge.
    filter :redcarpet,
      renderer: HTML,
      options: {
        fenced_code_blocks:  true,
        footnotes:           true,
        lax_spacing:         true,
        no_intra_emphasis:   true,
        space_after_headers: true,
        strikethrough:       true,
        superscript:         true,
        tables:              true,
        autolink:            true,
      },
      renderer_options: {
        with_toc_data: true
      },
      with_toc: include_toc

    filter :md_to_html_ext
    filter :admonition

    if item[:layout].nil?
      layout '/default.*'
    else
      layout "/#{item[:layout]}.*"
    end
  else
    layout '/redirect.*'
  end
end

compile '/**/*.scss' do
  filter :erb
  filter :sass,
    syntax: :scss,
    style: :compressed
  write item.identifier.without_ext + '-v' + rep.item[:version].to_s + '.css'
end

compile '/index.*' do
  filter :erb
  layout '/home.*'
  write '/index.html'
end

compile '/sitemap.*' do
  filter :erb
  write '/sitemap.xml'
end

compile '/robots.*' do
  filter :erb
  write '/robots.txt'
end

route '/**/*.{html,md}' do
  if item.identifier =~ '/index.*'
    '/index.html'
  else
    item.identifier.without_ext + '.html'
  end
end

compile '/**/*' do
  unless item.identifier.ext == 'scss'
    write item.identifier.to_s
  end
end

layout '/**/*', :erb

# Leave the favicon alone.
passthrough '/favicon.ico'
